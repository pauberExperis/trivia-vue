# trivia-vue

<img src="/public/trivia-logo-edited-png.png" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![heroku](https://img.shields.io/badge/%E2%86%91_Deploy_to-Heroku-7056bf.svg?style=flat)](https://trivialphil.herokuapp.com)
> Trivia Vue assignment

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install
```
npm install
```

## Develop
```
npm run serve
```

## Build
```
npm run build
```

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS